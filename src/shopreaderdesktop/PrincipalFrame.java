/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shopreaderdesktop;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingWorker;
import javax.swing.UnsupportedLookAndFeelException;
import org.pushingpixels.substance.api.skin.SubstanceNebulaLookAndFeel;
import shopreaderdesktop.iocard.NFCTools;
import shopreaderdesktop.views.CardModify;
import shopreaderdesktop.views.WithoutCard;

/**
 *
 * @author Camilo
 */
public class PrincipalFrame extends javax.swing.JFrame {

    /**
     * Creates new form PrincipalFrame
     */
    public final static int STATE_WAIT = 0;
    public final static int STATE_MODIFY = 1;
    public final static int STATE_ERROR = 2;
    
    
    private CardModify cardModify;
    private WithoutCard errorPanel, waitingPanel;
    private int state = 0;

    public PrincipalFrame() {
        initComponents();
        cardModify=new CardModify();
        errorPanel=new WithoutCard();
        errorPanel.withoutCardReader();
        waitingPanel=new WithoutCard();
        waitingPanel.withoutCard();
        setIconImage(new javax.swing.ImageIcon(getClass().getResource("/resources/shopreader.png")).getImage());
        add(new CardModify());
    }

    public void stateWaitingCard() {
        removeAll();
        add(waitingPanel);
        this.repaint();
        state = STATE_WAIT;
    }

    public void stateErrorReader() {
        removeAll();
        add(errorPanel);
        this.repaint();
        state = STATE_ERROR;
    }

    public void stateCardModify() {
        removeAll();
        add(cardModify);
        this.repaint();
        state = STATE_MODIFY;
    }

    public void stateCardModify(CardModify cm) {
        removeAll();
        add(cm);
        this.repaint();
        state = STATE_MODIFY;
    }

    public int getState() {
        return state;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ShopReader® Desktop");
        setMaximumSize(new java.awt.Dimension(480, 330));
        setMinimumSize(new java.awt.Dimension(480, 330));
        setPreferredSize(new java.awt.Dimension(480, 320));
        setResizable(false);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    static class NFCWorker extends SwingWorker<Integer, Void> {

        PrincipalFrame pf;

        public NFCWorker(PrincipalFrame pf) {
            this.pf = pf;
        }

        @Override
        protected Integer doInBackground() throws Exception {
            while (true) {
                try{
                    pf.stateWaitingCard();
                    NFCTools.cardPoll();
                    pf.stateCardModify();
                    Thread.sleep(1000);
                }catch(Exception ex){
                    pf.stateErrorReader();
                    Thread.sleep(5000);
                    System.exit(0);
                }
            }
        }
    }

    public static void main(String args[]) {
        try {
            javax.swing.UIManager.setLookAndFeel(new SubstanceNebulaLookAndFeel());
            JFrame.setDefaultLookAndFeelDecorated(true);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(PrincipalFrame.class.getName()).log(Level.SEVERE, null, ex);
        }


        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                PrincipalFrame pf=new PrincipalFrame();
                pf.setVisible(true);
                
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
