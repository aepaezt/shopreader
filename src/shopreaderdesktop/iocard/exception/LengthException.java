/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shopreaderdesktop.iocard.exception;

/**
 *
 * @author Andrés
 */
public class LengthException extends Exception {

    public LengthException() {
        super("La longitud del mensaje supera los 40 caracteres");
    }
}
