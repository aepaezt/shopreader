/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shopreaderdesktop.iocard;

import java.util.Arrays;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;
import shopreaderdesktop.iocard.exception.LengthException;

/**
 *
 * @author Andrés
 */
public class NFCTools {

    private static final byte[] CARD_POLL = {(byte) 0xFF, 0x00, 0x00, 0x00,
        0x04, (byte) 0xD4, 0x4A, 0x01, 0x00};
    private static final byte[] READ = {(byte) 0xFF, 0x00, 0x00, 0x00, 0x05,
        (byte) 0xD4, 0x40, 0x01, 0x30};
    private static final byte[] HEADER = {0x03, 0x2d, (byte) 0xD1, 0x01, 0x29,
        0x54, 0x02};
    private static final byte[] WRITE = {(byte) 0xFF, 0x00, 0x00, 0x00, 0x09,
        (byte) 0xD4, 0x40, 0x01, (byte) 0xA2};

    public static void cardPoll() throws CardException {
        TerminalFactory terminalFactory = TerminalFactory.getDefault();
        CardTerminal cardTerminal = terminalFactory.terminals().list().get(0);
        Card card = cardTerminal.connect("*");
        CardChannel cardChannel = card.getBasicChannel();
        cardChannel.transmit(new CommandAPDU(CARD_POLL));
        card.disconnect(false);
    }

    public static byte[] readCard() throws CardException {
        byte[] data = new byte[48];
        byte[] command = Arrays.copyOf(READ, READ.length + 1);
        ResponseAPDU response;
        command[READ.length] = 0x04;
        TerminalFactory terminalFactory = TerminalFactory.getDefault();
        CardTerminal cardTerminal = terminalFactory.terminals().list().get(0);
        Card card = cardTerminal.connect("*");
        CardChannel cardChannel = card.getBasicChannel();
        cardChannel.transmit(new CommandAPDU(CARD_POLL));
        response = cardChannel.transmit(new CommandAPDU(command));
        for (int i = 0; i < 16; i++) {
            data[i] = response.getBytes()[3 + i];
        }
        command[READ.length] = 0x08;
        response = cardChannel.transmit(new CommandAPDU(command));
        for (int i = 0; i < 16; i++) {
            data[16 + i] = response.getBytes()[3 + i];
        }
        command[READ.length] = 0xC;
        response = cardChannel.transmit(new CommandAPDU(command));
        for (int i = 0; i < 16; i++) {
            data[32 + i] = response.getBytes()[3 + i];
        }
        card.disconnect(false);
        return data;
    }

    public static void writeCard(String data) throws CardException, LengthException {
        byte[] hexData;
        byte[] command = Arrays.copyOf(WRITE, WRITE.length + 5);
        if (HEADER.length + data.length() + 1 <= 48) {
            hexData = new byte[48];
            for (int i = 0; i < hexData.length; i++) {
                if (i < HEADER.length) {
                    hexData[i] = HEADER[i];
                } else if (i < HEADER.length + data.length()) {
                    hexData[i] = (byte) data.charAt(i - HEADER.length);
                } else {
                    hexData[i] = 0x00;
                }
            }
            hexData[47] = (byte) 0xFE;
        } else {
            throw new LengthException();
        }
        TerminalFactory terminalFactory = TerminalFactory.getDefault();
        CardTerminal cardTerminal = terminalFactory.terminals().list().get(0);
        Card card = cardTerminal.connect("*");
        CardChannel cardChannel = card.getBasicChannel();
        cardChannel.transmit(new CommandAPDU(CARD_POLL));
        for (int i = 4; i < 16; i++) {
            command[WRITE.length] = (byte) (i & 0xFF);
            for (int j = 0; j < 4; j++) {
                int k = (i - 4) * 4;
                command[WRITE.length + j + 1] = hexData[k + j];
            }
            cardChannel.transmit(new CommandAPDU(command));
        }
        card.disconnect(false);
    }
}
