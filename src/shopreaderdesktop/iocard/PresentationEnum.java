package shopreaderdesktop.iocard;

public enum PresentationEnum {

    gramo(0, "gr", "gramos"),
    libra(1, "lb", "libras"),
    kilogramo(2, "kg", "kilogramos"),
    onza(3, "oz", "onzas"),
    mililitro(4, "ml", "mililitros"),
    litro(5, "lt", "litros"),
    cc(6, "cc", "centrimetros cubicos"),
    unidades(7, "unds", "unidades");
    private int id;
    private String label;
    private String text;

    PresentationEnum(int id, String label, String text) {
        this.id = id;
        this.label = label;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getText() {
        return text;
    }
}