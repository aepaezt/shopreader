/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shopreaderdesktop.views;

import javax.swing.ImageIcon;

/**
 *
 * @author Camilo
 */
public class WithoutCard extends javax.swing.JPanel {

    /**
     * Creates new form WithoutCard
     */
    ImageIcon errorImage, waitingImage ;
            
    public WithoutCard() {
        errorImage= new javax.swing.ImageIcon(getClass().getResource("/resources/error.png"));
        waitingImage= new javax.swing.ImageIcon(getClass().getResource("/resources/waiting.png"));
        initComponents();
    }
    
    public void withoutCardReader(){
        jLabel1.setIcon(errorImage);
    }
    
    public void withoutCard(){
        jLabel1.setIcon(waitingImage);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(480, 320));
        setMinimumSize(new java.awt.Dimension(480, 320));
        setPreferredSize(new java.awt.Dimension(480, 320));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/waiting.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
